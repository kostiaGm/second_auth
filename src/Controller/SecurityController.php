<?php

namespace App\Controller;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class SecurityController extends AbstractController
{
    /**
     * @Route("/api/login-check", name="user_login_check", methods={"POST"})
     */
    public function loginCheck(Request $request, JWTTokenManagerInterface $tokenManager, EventDispatcherInterface $dispatcher)
    {
        /*
        $postData = $request->get('data');

        if (empty($postData)) {
            throw ...
        }*/

        $user = $this->getUser();

        $dispatcher->addListener(Events::JWT_CREATED, function (JWTCreatedEvent $event) {
            $payload  = $event->getData();
            $payload['is_second_auth'] = true;
            $event->setData($payload);
        });

        return $this->json([
            'user' => $user->getRoles(),
            'token' => $tokenManager->create($user)
        ]);
    }

    /**
     * @Route("/user/test-new-token", name="test_new_token")
     *
     */
    public function testNewToken()
    {
        $user = $this->getUser();

       return $this->json(
           ['user' => !$user ?null:$user->getRoles() ]
           );
    }
}
