<?php
/*
 *symfony console doctrine:fixtures:load --purge-with-truncate --env=dev
 */
namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class User extends Fixture implements ContainerAwareInterface
{


    private UserManagerInterface $userManager;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->userManager = $container->get('fos_user.user_manager');
    }


    public function load(ObjectManager $manager)
    {

        $user = new \App\Entity\User();
        $user->setUsername("testuser");
        $user->setFullname("Test user");
        $user->setEmail("test@user.com");
        $user->setPlainPassword("123123");

        $this->userManager->updateUser($user);


        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
